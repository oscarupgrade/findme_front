import React, { useEffect } from 'react';
import { useDispatch,  } from "react-redux"; //useSelector
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { setUser,  } from "./app/appSlice"; //selectUser
import { checkSession } from './api/auth';

import "./App.scss";

//SECURE
// import SecureRoute from './SecureRoute';

//PAGES
import HomePage from './pages/HomePage';
import LoadingPage from './pages/LoadingPage';
import ProfilePage from './pages/ProfilePage';
import AddObject from './pages/AddObject';

//COMPONENTS
import Login from './components/Login';
import Register from './components/Register';
import EmailForPassword from './components/EmailForPassword';
import MessageAfterEmail from './components/MessageAfterEmail';
import RecoveredPassword from './components/RecoveredPassword/RecoveredPassword';

function App(props) {

  const dispatch = useDispatch();
  // const user = useSelector(selectUser);

  useEffect(() => {
    checkUserSession()
  });
  
  const checkUserSession = async () => {
      try {
        const data = await checkSession();
        delete data.password;
        dispatch(setUser(data));
      } catch (error) {
          dispatch(setUser(null));
      }
  };

  

  return (
    <Router>
      
        <Switch>
          <Route path="/" exact component={LoadingPage} />

          {/* <SecureRoute 
            // hasUser={hasUser}
            path="/home"
            exact component={(props) => (
              <HomePage {...props} />
            )}
          />
          <Route   /> */}
          <Route path="/home" exact component={HomePage} />
          <Route path="/profile" exact component={ProfilePage} />
          <Route path="/login" exact component={Login} />
          <Route path="/register" exact component={Register} />
          <Route path="/forgot/sendEmail" exact component={EmailForPassword} />
          <Route path="/forgot/recoverPassword" exact component={MessageAfterEmail} />
          <Route path="/forgot/passwordChange" exact component={RecoveredPassword} />
          <Route path="/form/addObject" exact component={AddObject} />
        </Switch>
    </Router>
  );
}

export default App;
