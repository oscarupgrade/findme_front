import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {useSelector} from 'react-redux';
import PropTypes from 'prop-types';

const SecureRoute = (props) => {
    const hasUser = useSelector((state) => state.user.hasUser);
    return hasUser ? <Route {...props} /> : <Redirect to= "/login" />;
}

SecureRoute.prototype = {
    hasUser: PropTypes.bool,
}

export default SecureRoute;
