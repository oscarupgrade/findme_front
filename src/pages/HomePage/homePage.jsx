import {useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";
import Footer from '../../components/Footer';


 const homePage = (props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const user = useSelector(selectUser);

    return (
        <div>
            {user?.data && <h1 className='home__hello'>¡FindMe {user.data.userName}!</h1>}
            <Footer />
        </div>
    )
};

export default homePage;
