import React, { useState } from "react";
// import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setObject } from "../../app/appSlice";
import { objectsAdd } from "../../api/object";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import "./AddObject.scss";

const INITIAL_STATE = {
  group: "",
  name: "",
  image: "",
  description: "",
};

function AddObject(props) {
  const dispatch = useDispatch();
  // const [ preview, setPreview ] = useState(null);
  const [state, setState] = useState(INITIAL_STATE);

  async function handleSubmitForm(ev) {
    ev.preventDefault();
    const { history } = props;

    try {
      // const formData = new FormData();

      // formData.append('image', state.image);
      // formData.append('name', state.name);
      // formData.append('group', state.group);
      // formData.append('description', state.description);

      const data = await objectsAdd(state);
      dispatch(setObject(data));

      setState(INITIAL_STATE);
      history.push("/home");
    } catch (error) {
      setState({ ...state, error: error.message });
    }
  };


  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;
    
    setState({
      ...state,
      [name]: value,
      
    });

  //   if (name === 'image') {
  //     let reader = new FileReader();
  //     let file = ev.target.files[0];

  //     reader.onloadend = () => {
  //         setState({
  //             ...state,
  //             //  image: file,
  //         });
  //         setPreview(reader.result);
  //     }
  //     reader.readAsDataURL(file);
  // }else {
  //     setState({
  //         ...state,
  //         [name]: value,
  //     });
  // }


  };

  return (
    <>
      <form onSubmit={handleSubmitForm}>
        <h1>Consejos</h1>

        <label htmlFor="group">
          <select
            type="text"
            name="group"
            value={state.group}
            onChange={handleChangeInput} //'electrónica', 'llaves', 'transporte', 'documentos personales', 'dinero', 'ropa', 'otros'
          >
            <option>-</option>
            <option>electrónica</option>
            <option>llaves</option>
            <option>transporte</option>
            <option>documentos personales</option>
            <option>dinero</option>
            <option>ropa</option>
            <option>otros</option>
          </select>
        </label>

        <label htmlFor="name">
          <input
            type="text"
            name="name"
            placeholder="Nombre del objeto"
            value={state.name}
            onChange={handleChangeInput}
          />
        </label>

        <div>
          <label htmlFor="image">
            <div>
              <input
                type="file"
                name="image"
                value={state.image}
                onChange={handleChangeInput}
                accept="image/png, image/jpg"
              />
            </div>
          </label>
        </div>

        <div>
          <label htmlFor="description">
            <div>
              <textarea
                type="text"
                name="description"
                placeholder="Describe el objeto"
                value={state.description}
                onChange={handleChangeInput}
              />
            </div>
          </label>
        </div>
        
        <div>
          <button type="submit">Subir Objeto</button>
        </div>
      </form>
    </>
  );
}

export default AddObject;
