import React from 'react';
import Lottie from 'lottie-react-web';
import lf30_editor_yrvelnlg from '../../animation/lf30_editor_yrvelnlg.json';
// import { setLocationPage } from "../../app/appSlice";
// import { useDispatch } from "react-redux";

import './LoadingPage.scss';

function LoadingPage(props) {
  // const dispatch = useDispatch();
    const { history } = props;
    const loading = () =>{
      // dispatch(setLocationPage("login"))
      history.push('/login');
    }
    setTimeout(loading, 3000);

    return (
        <div className='animation' >
          <div className='animation__title'>
          </div>
         <div className='animation__clip'>
           <Lottie options={{ animationData: lf30_editor_yrvelnlg}} />
         </div>
        </div>
    )
}

export default LoadingPage
