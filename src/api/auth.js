// const registerUrl = `https://rythme-project.herokuapp.com/auth/register`;
// const loginUrl = `https://rythme-project.herokuapp.com/auth/login`;
// const checkSessionUrl = `https://rythme-project.herokuapp.com/auth/check_session`;
// const logoutUrl = `https://rythme-project.herokuapp.com/auth/logout`;
//4000
const registerUrl = "http://localhost:4000/auth/register"; //4000
const loginUrl = "http://localhost:4000/auth/login";
const checkSessionUrl = "http://localhost:4000/auth/check-session";
const logoutUrl = "http://localhost:4000/auth/logout";
const emailPasswordUrl = "http://localhost:4000/forgot/sendemail";

export const register = async (userData) => {
    const request = await fetch(registerUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(userData),
    });

    const response = await request.json();

     if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};

export const login = async (userData) => {
    const request = await fetch(loginUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include', //para crear la cookie de sesion debe de estar a true
        body: JSON.stringify(userData),
    });

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
      }

      return response;

    }

export const checkSession = async () => {
          const request = await fetch(checkSessionUrl, {
              method: 'GET',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin':"*"
              },
              credentials: 'include',
          });

          const response = await request.json();

            if(!request.ok) {
            throw new Error(response.message);
         }
            return response;
}


export const logout = async () => {
    const request = await fetch(logoutUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':"*"
        },
            credentials: 'include',
    });

        const response = await request.json();

          if(!request.ok) {
          throw new Error(response.message);
       }
          return response;
}

export const emailPassword = async (userData) => {
    const request = await fetch(emailPasswordUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true, //para crear la cookie de sesion debe de estar a true
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include', 
        body: JSON.stringify(userData),
    });

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
      }

      return response;

    }
