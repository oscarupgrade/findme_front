
const objectUrl = "http://localhost:4000/object/add-object";

export const objectsAdd = async (data) => {
    const request = await fetch(objectUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(data),
    });

    const response = await request.json();

     if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};