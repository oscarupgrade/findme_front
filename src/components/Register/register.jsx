import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setUser } from "../../app/appSlice";
import { register } from "../../api/auth";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import "./register.scss";

const INITIAL_STATE = {
  email: "",
  password: "",
  passwordConfirm: "",
  userName: "",
};


const Register = (props) => {
  const dispatch = useDispatch();

  const [state, setState] = useState(INITIAL_STATE);
  const [show, setShow] = useState(false);
  const [show2, setShow2] = useState(false);

  async function handleSubmitForm(ev) {
    ev.preventDefault();
    const { history } = props;
    try {
      const data = await register(state);
      dispatch(setUser(data));

      setState(INITIAL_STATE);
      history.push("/home");
    } catch (error) {
      setState({ ...state, error: error.message });
    }
  }
  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;
    setState({
      ...state,
      [name]: value,
    });
  }

  function handleChangeInput2(ev) {
    validPassword()
    const name = ev.target.name;
    const value = ev.target.value;
    setState({
      ...state,
      [name]: value,
    });
  }

  const validPassword = () => { 
    if(state.password.length > 0){
      if(state.password !== state.passwordConfirm){
        console.log('las contraseñas no coinciden')
      } else {
        console.log('las contraseñas coinciden')
      }
    }
  }

  return (
    <form onSubmit={handleSubmitForm}>
      <h1>FindMe Register</h1>

      <label htmlFor="userName">
        <input
          type="text"
          name="userName"
          placeholder="userName"
          value={state.userName}
          onChange={handleChangeInput}
        />
      </label>

      <label htmlFor="email">
        <input
          type="email"
          name="email"
          placeholder="Email"
          value={state.email}
          onChange={handleChangeInput}
        />
      </label>

      <div>
        <label htmlFor="password">
          <div>
            <input
              type= {show2 ? 'text':'password'} 
              name="password"
              placeholder="Contraseña"
              value={state.password}
              onChange={handleChangeInput}
            />
            <FontAwesomeIcon onClick={() => setShow2(!show2)} icon={show2 ? faEyeSlash: faEye} />
          </div>
        </label>
      </div>

      <div>
        <label htmlFor="passwordConfirm">
          <div>
            <input
              type={show ? 'text':'password'}
              name="passwordConfirm"
              placeholder="Repetir Contraseña"
              value={state.passwordConfirm}
              onChange={handleChangeInput2}
  
            />
             <FontAwesomeIcon onClick={() => setShow(!show)} icon={show ? faEyeSlash: faEye } />
          </div>
        </label>
      </div>
      <div>
        <button type="submit">Registrarse</button>
      </div>

      <p>¿Ya tienes una cuenta?</p>
      <Link className="form__text" to="/login">
        <p>Iniciar sesión</p>
      </Link>
    </form>
  );
};

export default Register;
