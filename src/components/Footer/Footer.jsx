/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import {useSelector} from "react-redux";
import {selectUser} from "../../app/appSlice";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArchive } from "@fortawesome/free-solid-svg-icons";
import { faUserCircle, faPlusSquare } from "@fortawesome/free-regular-svg-icons";

import './Footer.scss';

function Footer() {
    
    const user = useSelector(selectUser);
    return (
        <div>
            <FontAwesomeIcon  icon={faArchive} />

            <Link to='/form/addObject'>
                <FontAwesomeIcon icon={faPlusSquare} />
            </Link>

            {user?.data?.img ? (<Link to='/profile'><img src={user?.data?.img} /></Link>)
             : 
           ( <Link to='/profile'>
                <FontAwesomeIcon className="profile" icon={faUserCircle} />
            </Link>)
            }
            
            
        </div>
    )
}


export default Footer
