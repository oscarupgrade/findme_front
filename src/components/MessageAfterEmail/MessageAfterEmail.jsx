import React from 'react'

const MessageAfterEmail = () => {
    return (
        <>
            <h1>Por favor, revisa tu correo electrónico</h1>
            <p>Te hemos enviado un mensaje por correo electrónico que te permitirá restablecer
            tu contraseña de manera rápida y fácil. Si no aparece en tu bandeja de entrada, revisa tambien las carpetas 
            "Spam" y/o "Correo no deseado"</p>
        </>
    )
}


export default MessageAfterEmail;
