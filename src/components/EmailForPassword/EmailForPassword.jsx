import React, { useState } from "react";
import { useDispatch } from "react-redux";
// import { Link } from "react-router-dom";
import { setUser } from "../../app/appSlice"; //setLocationPage
import { emailPassword } from "../../api/auth";

const EmailForPassword = (props) => {
  const dispatch = useDispatch();
  const [INITIAL_STATE, setINITIAL_STATE] = useState({
    email: "",
  });

  async function handleSubmitForm(ev) {
    ev.preventDefault();
    const { history } = props;
    try {
      const user = await emailPassword(INITIAL_STATE);
      dispatch(setUser(user));
    //    dispatch(setLocationPage("home"));
      history.push("/forgot/recoverPassword");
    } catch (error) {
      setINITIAL_STATE({ error: error.message });
    }
  }

  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;
    setINITIAL_STATE({
      ...INITIAL_STATE,
      [name]: value,
    });
  }
  return (
    <form onSubmit={handleSubmitForm}>
      <h1>He olvidado mi contraseña</h1>

      <p>No te preocupes, puedes establecer una nueva contraseña. Para ello introduce la dirección
      de correo electrónico con la que te registraste y pulsa en continuar</p>

      <label htmlFor="email">
        <input
          type="text"
          name="email"
          placeholder="Correo electrónico"
          value={INITIAL_STATE.email}
          onChange={handleChangeInput}
        />
      </label>

      <div>
        <button type="submit">Enviar</button>
      </div>
    </form>
  );
};

export default EmailForPassword;