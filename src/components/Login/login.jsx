import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { setLocationPage, setUser } from "../../app/appSlice";
import { login } from "../../api/auth";
import GoogleLogin from 'react-google-login';

import "./login.scss";

const Login = (props) => {
  const dispatch = useDispatch();
  const [INITIAL_STATE, setINITIAL_STATE] = useState({
    email: "",
    password: "",
  });

  async function handleSubmitForm(ev) {
    ev.preventDefault();
    const { history } = props;
    try {
      const user = await login(INITIAL_STATE);
      dispatch(setUser(user));
      dispatch(setLocationPage("home"));
      history.push("/home");
    } catch (error) {
      setINITIAL_STATE({ error: error.message });
    }
  }

  function handleChangeInput(ev) {
    const name = ev.target.name;
    const value = ev.target.value;
    setINITIAL_STATE({
      ...INITIAL_STATE,
      [name]: value,
    });
  }

  const responseGoogle = () => {
    const { history } = props;
      dispatch(setLocationPage("home"));
      history.push("/home");
  }

  return (
    <>
    <form onSubmit={handleSubmitForm}>
      <h1>FindMe Login</h1>
      <label htmlFor="email">
        <input
          type="text"
          name="email"
          placeholder="Correo electrónico"
          value={INITIAL_STATE.email}
          onChange={handleChangeInput}
        />
      </label>
      <label htmlFor="password">
        <input
          type="password"
          name="password"
          placeholder="Contraseña"
          value={INITIAL_STATE.password}
          onChange={handleChangeInput}
        />
      </label>
      
      {/* {INITIAL_STATE.error && <p className="error">{INITIAL_STATE.error}</p>} */}

      <div>
        <button type="submit">Iniciar Sesión</button>
      </div>
      ,<p>¿Todavía no tienes una cuenta?</p>
      <Link className="form__text" to="/register">
        <p>Regístrate</p>
      </Link>
      <Link className="form__text" to="/forgot/sendEmail">
        <p>Recuperar Contraseña</p>
      </Link>
    </form>

    <GoogleLogin
    clientId="932170048347-9m06n1lv8qer2eiq9g2v8jfuvgon14he.apps.googleusercontent.com"
    buttonText="Iniciar Sesión"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />
    </>
  );
};

export default Login;
