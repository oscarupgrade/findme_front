export const setUserReducer = (state, action) => {
    state.user = action.payload;
}

export const setLocationPageReducer = (state, action) => {
    state.locationPage = action.payload;
};

export const setObjectReducer = (state, action) => {
    state.object = action.payload;
};
