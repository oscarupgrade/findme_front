import { createSlice } from "@reduxjs/toolkit";
import { setUserReducer, setLocationPageReducer, setObjectReducer} from "./appReducers";

export const appSlice = createSlice( {
    name: 'app',
    initialState: {
        user : {data: null, message: null},
        locationPage: 'home',
        object:
        {
            name: '',
            description: '',
            image: '',
            group: []
        }
    },
    reducers: {
        setUser: setUserReducer,
        setLocationPage: setLocationPageReducer,
        setObject: setObjectReducer
    }
}
);

export const {
    setUser,
    setLocationPage,
    setObject
} = appSlice.actions;

export const selectUser = (state) => state.app.user;
export const selectLocation = (state) => state.app.locationPage;
export const selectObject = (state) => state.app.object;

export default appSlice.reducer;